package com.das.alex.androidtasksecond;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Calendar;

public class AboutMeActivity extends AppCompatActivity {

    String valueUserName;
    TextView userName, userAmountYear, userAmountDay, userAmountSecond, userZodiac;
    int valueUserDayBirthday, valueUserMonthBirthday, valueUserYearBirthday, valueUserSecond;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
        if (getIntent() != null && getIntent().getExtras() != null){
            valueUserName = getIntent().getStringExtra("userName");
            valueUserSecond = getIntent().getIntExtra("userSeconds", 0);
            valueUserDayBirthday = getIntent().getIntExtra("userDayBirthday", 0);
            valueUserMonthBirthday = getIntent().getIntExtra("userMonthBirthday", 0);
            valueUserYearBirthday = getIntent().getIntExtra("userYearBirthday", 0);
        }
        userName = findViewById(R.id.user_name_view);
        userName.setText(valueUserName);
        userAmountYear = findViewById(R.id.amount_year_view);
        userAmountYear.setText(String.valueOf( getYearUser()));
        userAmountDay = findViewById(R.id.amount_day_view);
        userAmountDay.setText(String.valueOf(getDayUser()));
        userAmountSecond = findViewById(R.id.amount_second_view);
        userZodiac = findViewById(R.id.user_zodiac_view);
        userZodiac.setText(getZodiac(valueUserMonthBirthday, valueUserDayBirthday));
        userAmountSecond.setText(String.valueOf(getSecondUser()));
    }

    private int getYearUser() {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - valueUserYearBirthday;
    }

    private int getDayUser() {
        return getYearUser()*365;
    }

    public String[] zodiacSigns = new String[]
    {
        "Capricorn","Aquarius","Pisces","Aries","Taurus","Gemini",
        "Cancer","Leo","Virgo","Libra",
        "Scorpio","Sagittarius"
    };

    public String getZodiac(int month, int day)
    {
        if      ((month == 12 && day >= 22 && day <= 31) || (month ==  1 && day >= 1 && day <= 19))
            return "Capricorn";
        else if ((month ==  1 && day >= 20 && day <= 31) || (month ==  2 && day >= 1 && day <= 17))
            return "Aquarius";
        else if ((month ==  2 && day >= 18 && day <= 29) || (month ==  3 && day >= 1 && day <= 19))
            return "Pisces";
        else if ((month ==  3 && day >= 20 && day <= 31) || (month ==  4 && day >= 1 && day <= 19))
            return "Aries";
        else if ((month ==  4 && day >= 20 && day <= 30) || (month ==  5 && day >= 1 && day <= 20))
            return "Taurus";
        else if ((month ==  5 && day >= 21 && day <= 31) || (month ==  6 && day >= 1 && day <= 20))
            return "Gemini";
        else if ((month ==  6 && day >= 21 && day <= 30) || (month ==  7 && day >= 1 && day <= 22))
            return "Cancer";
        else if ((month ==  7 && day >= 23 && day <= 31) || (month ==  8 && day >= 1 && day <= 22))
            return "Leo";
        else if ((month ==  8 && day >= 23 && day <= 31) || (month ==  9 && day >= 1 && day <= 22))
            return "Virgo";
        else if ((month ==  9 && day >= 23 && day <= 30) || (month == 10 && day >= 1 && day <= 22))
            return "Libra";
        else if ((month == 10 && day >= 23 && day <= 31) || (month == 11 && day >= 1 && day <= 21))
            return "Scorpio";
        else if ((month == 11 && day >= 22 && day <= 30) || (month == 12 && day >= 1 && day <= 21))
            return "Sagittarius";
        else
            return "Illegal date";
    }


    private int getSecondUser() {
        Calendar calendar = Calendar.getInstance();
        int currentSeconds = calendar.get(Calendar.SECOND);
        return currentSeconds - valueUserSecond;
    }

}
