package com.das.alex.androidtasksecond;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    EditText inputUserName;
    TextView inputUSerDateBirthday;
    Button buttonAboutMe;
    private int monthBirth;
    private int yearBirth;
    private int dayBirth;
    private DatePickerDialog.OnDateSetListener mDateSetlistener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputUserName = findViewById(R.id.user_name_edit_text);
        inputUSerDateBirthday = findViewById(R.id.user_data_birthday_edit_text);
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdfDate = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
        String formatedDate = sdfDate.format(cal.getTime());
        inputUSerDateBirthday.setText(formatedDate);
        inputUSerDateBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateBirthday();
            }
        });
        buttonAboutMe = findViewById(R.id.button_about_me);

        buttonAboutMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(inputUserName.getText().toString().trim()) &&
                        dayBirth != 0 && monthBirth != 0 && yearBirth != 0){
                    Intent intent = new Intent(MainActivity.this, AboutMeActivity.class);
                    intent.putExtra("userName", inputUserName.getText().toString().trim());
                    Calendar calendar = Calendar.getInstance();
                    int seconds = calendar.get(Calendar.SECOND);
                    intent.putExtra("userSeconds", seconds);
                    intent.putExtra("userDayBirthday", dayBirth);
                    intent.putExtra("userMonthBirthday", monthBirth);
                    intent.putExtra("userYearBirthday", yearBirth);
                    startActivity(intent);    
                }else{
                    Toast.makeText(MainActivity.this, "Please, input all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getDateBirthday(){
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(MainActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mDateSetlistener,
                year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        mDateSetlistener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal2 = new GregorianCalendar();
                cal2.set(year, month, dayOfMonth);
                yearBirth = year;
                monthBirth = month + 1;
                dayBirth = dayOfMonth;
                SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
                String formatedDate = sdf.format(cal2.getTime());
                Log.d("new Date Start", String.valueOf(cal2));
                inputUSerDateBirthday.setText(formatedDate);
            }
        };
    }
}
